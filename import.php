<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import File</title>
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container">
    <h3>Import users from a CSV FILE</h3>
        <div class="row">
            <div class="col-sm-6">
                <p>CSV file</p>
                <?php
                // var_dump(phpinfo());die;
                    include_once 'dbConfig.php';
                    $startTime = microtime();
                    if (isset($_POST['importSubmit'])) {

                        $target_file = $_FILES["myfile"]["tmp_name"];
                        $handle = fopen ($target_file, "r");
                        $csvMimes = array(
                            'application/x-csv',
                            'text/x-csv',
                            'text/csv',
                            'application/csv',
                        );
                        // check file CSV
                        if (in_array($_FILES['myfile']['type'], $csvMimes)) {
                            echo "<b>File is CSV</b>";
                            echo "</br>";
                            $line=1;
                            $arr = array(); 
                            $users_list = array();
                            if (!empty($handle)) {
                                $obj = new TransactionDB();
                                $obj->importdata($data,$arr,$handle);
                            }   
                            fclose ($handle);
                        } else {
                            echo "<b>File is not CSV</b>";
                        }
                    }
                ?>
            </div>
            <div class="col-6">
                <form action="import.php" method="post" enctype="multipart/form-data">
                    <input type="file" name="myfile" id="myfile"> <br><br><br>
                    <input type="submit" name="importSubmit" value="Import" class="btn btn-success">
                    <br>
                    <br>
                    <p><a href="index.php" class="btn btn-primary">Back</a></p>
                </form>
            </div>
        </div>
    </div>
</body>

</html>