<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import - Export file CSV</title>
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
</head>
<body>
<?php
        include_once 'dbConfig.php';
    ?>
        <br/><br />
        
            <div class="container" style="width:900px;">
                <h2>Import - Export Table Data to CSV file</h2>
                <br />
        <form method="post" action="import.php">
            <input type="submit" name="Import" value="Import" class="btn btn-primary" />
        </form>
            </br>   
        <form method="post" action="export.php">
            <input type="submit" name="Export" value="Export" class="btn btn-success" />
        </form><br>
        
        <br />
        <div class="table-responsive" id="employee_table">
            <table class="table table-bordered">
                <tr>
                    <th width="5%">ID</th>
                    <th width="25%">Firstname</th>
                    <th width="25%">Lastname</th>
                    <th width="35%">Username</th>
                    <th width="15%">Email</th>
                    <th width="20%">Password</th>
                </tr>
            <?php
                $obj = new TransactionDB();
                $query = "SELECT * FROM users ORDER BY id ASC";
                $result = $obj->pdo->prepare($query);
                $result->execute();
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    $id = $row['id'];
                    $firstname = $row['firstname'];
                    $lastname = $row['lastname'];
                    $username = $row['username'];
                    $email = $row['email'];
                    $password = $row['passw'];
                ?>
            <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $firstname; ?></td>
                <td><?php echo $lastname; ?></td>
                <td><?php echo $username; ?></td>
                <td><?php echo $email; ?></td>
                <td><?php echo $password; ?></td>
            </tr>
            <?php  
                }
            ?>
            </table>
        </div>
    </div>
</body>
</html>