#Baitap1
url : cms219.dev1.local

Cách sử dụng :
- Truy cập vào url sẽ hiện ra 2 chức năng là import, export và dữ liệu trên mysql được hiển thị ra
- Khi chọn import sẽ chuyển sang trang để import file CSV
+ Chuẩn bị 1 file csv có dữ liệu
+ Bấm Choose file để chọn file csv
+ Bấm import để đưa dữ liệu file lên mysql
- Khi chọn export sẽ tải về 1 file có tên export.csv về máy, trong file đó gồm tất cả dữ liệu đang có trên mysql
