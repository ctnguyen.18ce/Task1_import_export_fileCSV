<?php
class TransactionDB {

    const DB_HOST = 'localhost';
    const DB_NAME = 'dbname';
    const DB_USER = 'root';
    const DB_PASSWORD = 'cms-8341';
    /**
     * Open the database connection
     */
    public function __construct() {
        // open database connection
        $conn = sprintf("mysql:host=%s;dbname=%s", self::DB_HOST, self::DB_NAME);
        try {
            $this->pdo = new PDO($conn, self::DB_USER, self::DB_PASSWORD);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function importdata($data,$arr,$handle){
        try {
            $this->pdo->beginTransaction();

            $have_error = false;
            $line = 1;
            $arr = array();
            $sql = "DELETE FROM users";
            $this->pdo->exec($sql);
            $count = 1;
            $query = "";
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $arr[]=$data;
                if($line == 1 ){
                    // $line++;
                }else{
                    if (empty($data[0])) {
                        echo "Empty at line " . $line;
                        echo "</br>";
                        if (!$have_error) {
                            $have_error = true;
                        }
                    }
                    if (empty($data[3])) {
                        echo "Empty at ". $arr[0][3] ." and ID is ".$data[0];
                        echo "</br>";
                        if (!$have_error) {
                            $have_error = true;
                        }
                    }
                    if (empty($data[5])) {
                        echo "Empty at ". $arr[0][5] ." and ID is ".$data[0];
                        echo "</br>";
                        if (!$have_error) {
                            $have_error = true;
                        }
                    }
                    // if (!filter_var($data[4], FILTER_VALIDATE_EMAIL)) {
                    //     echo $data[4]. " : Not a valid email"."<br>";
                    //     if (!$have_error) {
                    //         $have_error = true;
                    //     }
                    // }
                    if ($have_error == true){
                        throw new PDOException();
                    }
                    if (!$have_error){
                        $firstname = $data[1];
                        $lastname = $data[2];
                        $username = $data[3];
                        $email = $data[4];
                        $passw = $data[5];

                        if ($count == 1) {
                            $query .= "INSERT INTO users (firstname,lastname,username,email,passw) VALUES('". $firstname . "','". $lastname ."','". $username ."','". $email ."','". $passw ."')";
                        } else {
                            $query .= ",('". $firstname . "','". $lastname ."','". $username ."','". $email ."','". $passw ."')";
                        }
                        if ($count == 50000) {
                            $this->pdo->exec($query);
                            $query = "";
                            $count = 0;
                        }
                        $count++;
                    }
                }
                $line ++;
            }
            $this->pdo->commit();
        }catch(PDOException $e) {
            $this->pdo->rollBack();
        }
    }
}
?>