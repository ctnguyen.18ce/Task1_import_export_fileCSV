<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Export</title>
</head>

<body>
    <?php
        include_once 'dbConfig.php';
        $obj = new TransactionDB();

        $header = array('id','firstname','lastname','username','email','password');

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=file_export.csv');

        $output = fopen( 'php://output', 'w' );
        ob_end_clean();

        fputcsv($output, $header);
        $query = "SELECT * FROM users";
        $result = $obj->pdo->prepare($query);
        $result->execute(); 
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $data) {
            fputcsv($output, $data);
        }
        exit();
    ?>
</body>

</html>